$csv = .\deploy-cdportgroups.csv
$logfile =
$vcentercred = (Get-Credential -message "vcenter credentials") 

$vdportgroups = Import-Csv -UseCulture $csv

foreach ($vdportgroup in $vdportgroups){ 
    $vdportcheck = get-vdswitch $vdportgroup.vdswitch -location $vdportgroup.location | get-VDPortgroup | where {$_.vlanid -eq $vdportgroup.vlanid}
    if ($vdportcheck -ne $null){
    Write-Output "$(get-date) ERROR creating portgroup $($vdportgroup.Name) there is already a portgroup with that vlandID $vdportcheck" | Out-File $logfile -Append
    }
    else{
    Write-Output "$(get-date) INFO creating portgroup $($vdportgroup.Name)" | Out-File $logfile -Append
    get-vdswitch $vdportgroup.vdswitch -location $vdportgroup.location | New-VDPortgroup -Name `
    $vdportgroup.Name -VlanId $vdportgroup.vlanid
    }
        
    }