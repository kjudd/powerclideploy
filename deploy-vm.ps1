﻿#collect variables from user 
$vcentercred = (Get-Credential -message "vcenter credentials") 
$guestcred = (Get-Credential-message "guest credentials") 
#path to csv file
$csvinput =

#import csv
$csv = import-csv $csvinput -UseCulture
$logfile =

#starting log file
Write-Output "*****************************************" `r`n | Out-File $logfile -Append^M
Write-Output "$(get-date) INFO deployment beginning " | Out-File $logfile -Append


#import vmware powershell snapin
add-pssnapin vmware* 

#connect to each vcenter
$vcenter = $csv.vcenter | select -Unique 
foreach ($vcenter in $vcenter){
    connect-viserver -Credential $vcentercred -Server $vcenter -WarningAction SilentlyContinue
    }

#deploy VMs
foreach ($deploy in $csv){
    $vm_unique =  get-cluster -name $deploy.cluster -server $deploy.vcenter | get-vm -Name $deploy.vmname

    if ($vm_unique -ne $null){
        echo "whoa there cowboy this name is taken by $vm_unique"
        Write-Output "$(get-date) ERROR creating vm $($deploy.vnname) and there is already a VM with that name $vm_unique" | Out-File $logfile -Append
        }
    else{
    Write-Output "$(get-date) INFO creating vm $($deploy.vnname)" | Out-File $logfile -Append
    get-cluster -name $deploy.cluster -server $deploy.vcenter | new-vm -Name $deploy.vmname -Datastore $deploy.datastore -template $deploy.template
    start-sleep -Seconds 3
}



#waiting for all the VM's to be provisioned 	
do {
    $toolsStatus = (Get-VM -name $csv.vmname).extensiondata.Guest.ToolsStatus
    Start-Sleep -Seconds 10
    } until  ($toolsStatus -match ‘toolsNotRunning’) 

#set memory and cpu
foreach($deploy in $csv){
    Write-Output "$(get-date) INFO setting $($deploy.numcpu) cpu/s to vm $($deploy.vnname)" | Out-File $logfile -Append
    get-vm -name $deploy.vmname | set-vm -numcpu $deploy.numcpu -confirm:$false 
    start-sleep -Seconds 3
    get-vm -name $deploy.vmname | set-vm -MemoryGB $deploy.memGB -confirm:$false 
    }

#connect network and boot
foreach($deploy in $csv){
    get-vm $deploy.vmname | Get-NetworkAdapter | Set-NetworkAdapter -NetworkName $deploy.portgroup1 -Confirm:$false -StartConnected:$true  -Type Vmxnet3
    Start-Sleep -Seconds 3 
    get-vm $deploy.vmname | Start-VM
    }

#wait for vm to boot
do {
    $toolsStatus = (Get-VM -name $csv.vmname).extensiondata.Guest.ToolsStatus
    Start-Sleep -Seconds 5
    } until  ($toolsStatus -match ‘toolsOK’)

Start-Sleep -Seconds 10

#configure static IP
foreach($deploy in $csv){
    Invoke-VMScript -VM $deploy.vmname -GuestCredential $guestcred -ScriptType Bash -ScriptText "nmcli con del ens192"
    Invoke-VMScript -VM $deploy.vmname -GuestCredential $guestcred -ScriptType Bash -ScriptText "nmcli con add type ethernet con-name $($deploy.con1) ifname ens192 ip4 $($deploy.ip1) gw4 $($deploy.gw1)"
    Invoke-VMScript -VM $deploy.vmname -GuestCredential $guestcred -ScriptType Bash -ScriptText "nmcli con mod $($deploy.con1) ipv4.dns `"$($deploy.dns1) $($deploy.dns2)`""
    Invoke-VMScript -VM $deploy.vmname -GuestCredential $guestcred -ScriptType Bash -ScriptText "nmcli con up $($deploy.con1)" 
    }

#add additional harddisk and create logical volume
foreach($deploy in $csv){
get-vm  $deploy.vmname | New-HardDisk -CapacityGB $deploy.disk1 -ThinProvisioned
Invoke-VMScript -VM $deploy.vmname -GuestCredential $guestcred -ScriptType Bash -ScriptText "vgcreate $($deploy.vg1) /dev/$($deploy.dev1)"
Invoke-VMScript -VM $deploy.vmname -GuestCredential $guestcred -ScriptType Bash -ScriptText "lvcreate -l $($deploy.lv1size)%VG -n $($deploy.lv1) $($deploy.vg1)"
Invoke-VMScript -VM $deploy.vmname -GuestCredential $guestcred -ScriptType Bash -ScriptText "mkfs.$($deploy.fs1) /dev/mapper/$($deploy.vg1)-$($deploy.lv1)"
Invoke-VMScript -VM $deploy.vmname -GuestCredential $guestcred -ScriptType Bash -ScriptText "echo `"/dev/mapper/$($deploy.vg1)-$($deploy.lv1)      $($deploy.mount1)       $($deploy.fs1)    defaults,noatime        1 2`" >> /etc/fstab"
Invoke-VMScript -VM $deploy.vmname -GuestCredential $guestcred -ScriptType Bash -ScriptText "mount $($deploy.mount1)"
}